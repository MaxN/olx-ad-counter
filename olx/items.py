# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class OlxCpuItem(scrapy.Item):
  # define the fields for your item here like:
  title = scrapy.Field()
  cost = scrapy.Field()
  image_urls = scrapy.Field()
  images = scrapy.Field()
  
  def to_csv(self):
    url = self.get('image_urls', 'NA')
    if url != 'NA':
      url = url[0]
    img_path = self.get('images', 'NA')
    if img_path != 'NA':
      img_path = img_path[0]['path']
      
    return "%s\t%s\t%s\t%s\n" % (self.get('title', 'NA'), self.get('cost', 'NA'), img_path, url)
