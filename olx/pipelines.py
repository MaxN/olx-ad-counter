# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from os import path

class SaveToCsvPipeline(object):

  def __init__(self, csv_store=''):
    self.fn = csv_store+'/main.csv'
    i = 0
    while path.exists(self.fn):
      self.fn = csv_store+'/main.{}.csv'.format(i)
      i += 1

  @classmethod
  def from_crawler(cls, crawler):
    settings = crawler.settings
    return cls(settings['CSV_STORE'])

  def open_spider(self, spider):
    self.file = open(self.fn, 'w')

  def close_spider(self, spider):
    self.file.close()

  def process_item(self, item, spider):
    self.file.write(item.to_csv())
    return item
