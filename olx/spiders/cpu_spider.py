import scrapy
from olx.items import OlxCpuItem
from olx.spiders.olx_spider import OlxSpider

class CpuSpider(OlxSpider):
  name = 'cpu'
  start_urls = ['https://www.olx.ua/elektronika/kompyutery-i-komplektuyuschie/komplektuyuschie-i-aksesuary/protsessory/?currency=USD']
  custom_settings = {
    'IMAGES_STORE': 'result/cpu',
    'CSV_STORE': 'result/cpu'
  }