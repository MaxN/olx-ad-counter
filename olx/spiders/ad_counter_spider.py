import scrapy

class AdCounterSpider(scrapy.Spider):
  name = 'adcounter'
  start_urls = ['https://www.olx.ua/elektronika/kompyutery-i-komplektuyuschie']
  item_count = 0
  def parse(self, response):
    for item in response.css('table[class*="offers--top"] [class*="promoted"] table tbody > tr:first-child'):
      self.item_count += 1
      yield {
        'url': item.css('a[class*="link"]::attr(href)').get(),
        'title': item.css('a[class*="link"] strong::text').get(),
        'price': item.css('[class*="price"] strong::text').get()
      }
    for item in response.css('table#offers_table [class*="promoted"] table tbody > tr:first-child'):
      self.item_count += 1
      yield {
        'url': item.css('a[class*="link"]::attr(href)').get(),
        'title': item.css('a[class*="link"] strong::text').get(),
        'price': item.css('[class*="price"] strong::text').get()
      }
    next_url = response.css('a[class*="link"][data-cy="page-link-next"]::attr(href)').get()
    self.logger.info('item_count %d', self.item_count)
    if next_url is not None:
      yield response.follow(next_url, self.parse)