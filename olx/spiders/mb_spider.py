import scrapy
from olx.items import OlxCpuItem
from olx.spiders.olx_spider import OlxSpider

class MbSpider(OlxSpider):
  name = 'mb'
  start_urls = ['https://www.olx.ua/elektronika/kompyutery-i-komplektuyuschie/komplektuyuschie-i-aksesuary/materinskie-platy/?currency=USD']
  custom_settings = {
    'IMAGES_STORE': 'result/mb',
    'CSV_STORE': 'result/mb'
  }