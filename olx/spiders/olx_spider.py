import scrapy
from olx.items import OlxCpuItem

class OlxSpider(scrapy.Spider):
  name = 'olx'
  
  def parse(self, response):
    for item in response.css('table[class*="offers--top"] [class*="promoted"] table tbody > tr:first-child'):
      yield OlxCpuItem(
        title=item.css('a[class*="link"] strong::text').get(),
        cost=item.css('[class*="price"] strong::text').get(),
        image_urls = [item.css('img::attr(src)').get()]
        ) 

    for item in response.css('table#offers_table table tbody > tr:first-child'):
      yield OlxCpuItem(
        title=item.css('a[class*="link"] strong::text').get(),
        cost=item.css('[class*="price"] strong::text').get(),
        image_urls = [item.css('img::attr(src)').get()]
        ) 

    next_url = response.css('a[class*="link"][data-cy="page-link-next"]::attr(href)').get()
    
    if next_url is not None:
      yield response.follow(next_url, self.parse)